import initSidebarBundle from '~/sidebar/sidebar_bundle';
import initRelatedIssues from '~/related_issues';
import initShow from '../show';

initShow();
initSidebarBundle();
initRelatedIssues();
